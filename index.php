<!DOCTYPE html>
<html>
<head>
	<title>gallery</title>
	   <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
	   <link rel="stylesheet" href="js/fancyBox/source/jquery.fancybox.css" type="text/css" media="screen" />
	   <script type="text/javascript" src="js/fancyBox/source/jquery.fancybox.pack.js"></script>
	   <link rel="stylesheet" href="js/fancyBox/source/helpers/jquery.fancybox-buttons.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/fancyBox/source/helpers/jquery.fancybox-buttons.js"></script>
		<script type="text/javascript" src="js/fancybox/source/helpers/jquery.fancybox-media.js"></script>

		<link rel="stylesheet" href="js/fancyBox/source/helpers/jquery.fancybox-thumbs.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/fancyBox/source/helpers/jquery.fancybox-thumbs.js"></script>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

	   <link rel="stylesheet" type="text/css" href="css/custom.css">
</head>

<body class="bg">

	<?php
		include('upload.php');
	?>

	<div id="content">
	<form action="index.php" method="POST" enctype="multipart/form-data" class="form-inline">
		<label for="file">Choose File:</label>
		<br>
		<input type="file" name="file" class="form-control" id="file">
		<br>
		<br>
		<label for="file">Title:</label>
		<br>
		<input type="text" name="nam" class="form-control" id="nam">
		<br>
		<br>
		<button type="submit" class="btn btn-default" name="submit">Upload</button>
	</form>
	</div>

	<script>
		$(document).ready(function() {
			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed : 300,

				closeEffect : 'elastic',
				closeSpeed : 250,

				closeClick : true,
			});
		});
	</script>

	<img src="imgur.png" alt="not imgur">


</body>
</html>